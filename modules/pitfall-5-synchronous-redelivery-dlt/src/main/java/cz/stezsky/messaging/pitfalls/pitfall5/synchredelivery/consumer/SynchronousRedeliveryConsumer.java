package cz.stezsky.messaging.pitfalls.pitfall5.synchredelivery.consumer;

import cz.stezsky.messaging.pitfalls.api.OrderEvents.OrderEvent;
import cz.stezsky.messaging.pitfalls.bootstrap.consumer.KafkaConsumerConfiguration;
import cz.stezsky.messaging.pitfalls.pitfall5.synchredelivery.producer.SynchronousRedeliveryProducer;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.protocol.Message;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.ContainerCustomizer;
import org.springframework.kafka.core.KafkaOperations;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.DeadLetterPublishingRecoverer;
import org.springframework.kafka.listener.DefaultErrorHandler;
import org.springframework.kafka.support.ExponentialBackOffWithMaxRetries;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;

@SpringBootApplication
@Configuration
@Import(KafkaConsumerConfiguration.class)
@Slf4j
public class SynchronousRedeliveryConsumer {

	@Slf4j
	public static class ConsumerImpl {

		@KafkaListener(topics = SynchronousRedeliveryProducer.TOPIC, groupId = "${random.uuid}")
		public void onOrders(OrderEvent event, @Header(KafkaHeaders.DELIVERY_ATTEMPT) int delivery) {
			log.info("{} delivery {}", event, delivery);
			if (event.customerId().equals("Customer1")) {
				throw new IllegalArgumentException("Rollback");
				//throw new NullPointerException("Rollback");
			}
		}
	}
	//kafkacat -b localhost:29092 -t pitfall-5-redelivery-1-dlt -C -f '%p) %s\n'

	@Bean
	public ConsumerImpl consumerImpl() {
		return new ConsumerImpl();
	}

	@Bean
	public DefaultErrorHandler defaultErrorHandler(KafkaOperations<Object, Object> operations) {

		// Publish to dead letter topic any messages dropped after retries with back off
		var recoverer = new DeadLetterPublishingRecoverer(operations,
				// Always send to first/only partition of DLT suffixed topic
				(cr, e) -> new TopicPartition(cr.topic() + "-dlt", 0));

		var exponentialBackOff = new ExponentialBackOffWithMaxRetries(3);
		exponentialBackOff.setInitialInterval(1000);
		exponentialBackOff.setMultiplier(1.2);
		exponentialBackOff.setMaxInterval(10_000);

		var errorHandler = new DefaultErrorHandler(recoverer, exponentialBackOff);
		errorHandler.addNotRetryableExceptions(NullPointerException.class);
		return errorHandler;
	}
	@Bean
	ContainerCustomizer<String, Message, ConcurrentMessageListenerContainer<String, Message>> containerCustomizer(
			ConcurrentKafkaListenerContainerFactory<String, Message> factory) {

		ContainerCustomizer<String, Message, ConcurrentMessageListenerContainer<String, Message>> cust = container -> {
			container.getContainerProperties().setDeliveryAttemptHeader(true);
		};
		factory.setContainerCustomizer(cust);
		return cust;
	}

	public static void main(String[] args) {
		SpringApplication.run(SynchronousRedeliveryConsumer.class, args);
	}
}
