package cz.stezsky.messaging.pitfalls.pitfall5.synchredelivery.producer;

import cz.stezsky.messaging.pitfalls.api.OrderEvents.OrderEvent;
import cz.stezsky.messaging.pitfalls.bootstrap.producer.EventFile;
import cz.stezsky.messaging.pitfalls.bootstrap.producer.KafkaProducerConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.core.KafkaTemplate;

import java.nio.file.Path;

import static cz.stezsky.messaging.pitfalls.bootstrap.producer.SendResultLogger.LOG_EVENT;

@SpringBootApplication
@Import(KafkaProducerConfiguration.class)
@Slf4j
public class SynchronousRedeliveryProducer implements CommandLineRunner {
	//kafkacat -b localhost:29092 -t pitfall-5-redelivery-1 -C -f '%p) %s\n'
	public static final String TOPIC = "pitfall-5-redelivery-1";
	//public static final String TOPIC = "pitfall-5-redelivery-2";
	//public static final String TOPIC = "pitfall-5-redelivery-2";

	@Autowired
	private KafkaTemplate<String, OrderEvent> kafkaTemplate;

	@Override
	public void run(String... args){
		//Read events from file
		EventFile.readEvents(Path.of("events", "5-events.csv")).forEach(event -> {
			//Send to kafka
			kafkaTemplate.send(TOPIC, event.customerId(), event).whenComplete(LOG_EVENT);
		});
	}

	public static void main(String[] args) {
		SpringApplication.run(SynchronousRedeliveryProducer.class, args);
	}
}
