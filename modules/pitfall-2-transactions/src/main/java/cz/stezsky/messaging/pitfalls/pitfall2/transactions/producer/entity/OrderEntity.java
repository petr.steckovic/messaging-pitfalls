package cz.stezsky.messaging.pitfalls.pitfall2.transactions.producer.entity;

import cz.stezsky.messaging.pitfalls.api.OrderEvents.OrderStatus;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    private String orderId;

    private String customerId;

    private OrderStatus status;
}
