package cz.stezsky.messaging.pitfalls.pitfall2.transactions.producer;

import cz.stezsky.messaging.pitfalls.api.OrderEvents.OrderEvent;
import cz.stezsky.messaging.pitfalls.bootstrap.producer.EventFile;
import cz.stezsky.messaging.pitfalls.bootstrap.producer.KafkaProducerConfiguration;
import cz.stezsky.messaging.pitfalls.pitfall2.transactions.producer.entity.OrderEntity;
import cz.stezsky.messaging.pitfalls.pitfall2.transactions.producer.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.Path;

import static cz.stezsky.messaging.pitfalls.bootstrap.producer.SendResultLogger.LOG_EVENT;

@SpringBootApplication
@Import(KafkaProducerConfiguration.class)
@EnableTransactionManagement
@Slf4j
public class TransactionChainingProducer implements CommandLineRunner {
	//kafkacat -b localhost:29092 -t pitfall-2-transactions-1 -C -f '%p) %s\n'
	public static final String TOPIC = "pitfall-2-transactions-1";
	//public static final String TOPIC = "pitfall-2-transactions-2";
	//public static final String TOPIC = "pitfall-2-transactions-3";

	@Autowired
	private KafkaTemplate<String, OrderEvent> kafkaTemplate;

	@Autowired
	private OrderRepository orderRepository;

	@Override
	@Transactional("transactionManager")
	//@Transactional("kafkaTransactionManager")
	public void run(String... args) throws Exception{
		//Read events from file
		var event = EventFile.readEvents(Path.of("events", "1-event.csv")).get(0);

		//save to DB
		orderRepository.save(new OrderEntity(0L, event.orderId(), event.customerId(), event.status()));

		//Send to kafka
		kafkaTemplate.send(TOPIC, event).whenComplete(LOG_EVENT).get();

		//Rollback transaction
		throw new IllegalArgumentException("Rollback should proceed");
	}

	public static void main(String[] args) {
		SpringApplication.run(TransactionChainingProducer.class, args);
	}
}
