package cz.stezsky.messaging.pitfalls.pitfall2.transactions.producer.repository;

import cz.stezsky.messaging.pitfalls.pitfall2.transactions.producer.entity.OrderEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends CrudRepository<OrderEntity, Long> {
}
