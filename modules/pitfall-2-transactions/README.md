# Chained transaction pitfall

## Idea of example

By enabling kafka transaction manager, kafka transaction manager join to the transaction automatically. 

## Enable the kafka transaction in SpringBoot
First of all, we need to enable Spring Kafka's Kafka Transaction Manager. We can do it by simply setting the transactional id property in our `application.properties`:
```properties
spring.kafka.producer.transaction-id-prefix=tx-
```
By setting this property, Spring Boot will automatically configure the Kafka Transaction Manager. New bean named `kafkaTransactionManager` is in place in Application context.


## How to use the example

1. Run producer and check the transaction rolled back (no message in the topic `pitfall-2-transactions-1` and no record in `order_entity` table)
2. Remove IllegalStateException in source code
3. Run producer and check the transaction is committed (message in the topic `pitfall-2-transactions-1` and record in `order_entity` table)
4. Check the logs there is no 2PC. Only transaction chaining is used
