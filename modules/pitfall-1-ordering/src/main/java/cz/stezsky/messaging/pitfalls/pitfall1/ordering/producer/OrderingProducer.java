package cz.stezsky.messaging.pitfalls.pitfall1.ordering.producer;

import cz.stezsky.messaging.pitfalls.api.OrderEvents.OrderEvent;
import cz.stezsky.messaging.pitfalls.bootstrap.producer.EventFile;
import cz.stezsky.messaging.pitfalls.bootstrap.producer.KafkaProducerConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.core.KafkaTemplate;

import java.nio.file.Path;

import static cz.stezsky.messaging.pitfalls.bootstrap.producer.SendResultLogger.LOG_EVENT;

@SpringBootApplication
@Import(KafkaProducerConfiguration.class)
public class OrderingProducer implements CommandLineRunner {

	//kafkacat -b localhost:29092 -t pitfall-1-ordering-1 -C -f '%p) %s\n'
	public static final String TOPIC = "pitfall-1-ordering-1";
//	public static final String TOPIC = "pitfall-1-ordering-2";
//	public static final String TOPIC = "pitfall-1-ordering-3";
	@Autowired
	private KafkaTemplate<String, OrderEvent> kafkaTemplate;

	@Override
	public void run(String... args)  {
		//Read events from file
		var orderEvents = EventFile.readEvents(Path.of("events", "100-events.csv"));

		//Send all events to kafka
		orderEvents.forEach(event -> kafkaTemplate.send(TOPIC, event).whenComplete(LOG_EVENT));
	}

	public static void main(String[] args) {
		SpringApplication.run(OrderingProducer.class, args);
	}
}
