package cz.stezsky.messaging.pitfalls.pitfall1.ordering.consumer;

import cz.stezsky.messaging.pitfalls.api.OrderEvents.OrderEvent;
import cz.stezsky.messaging.pitfalls.api.OrderEvents.OrderStatus;
import cz.stezsky.messaging.pitfalls.bootstrap.consumer.KafkaConsumerConfiguration;
import cz.stezsky.messaging.pitfalls.pitfall1.ordering.producer.OrderingProducer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.annotation.KafkaListener;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@Configuration
@Import(KafkaConsumerConfiguration.class)
@Slf4j
public class OrderingConsumer  {

	@Slf4j
	public static class OrderingConsumerImpl {
		private final List<OrderEvent> receivedEvents = new ArrayList<>();

		/**
		 * Process order events and detect reordering and duplicities
		 */
		@KafkaListener(topics = OrderingProducer.TOPIC, groupId = "ordering-consumer")
		public synchronized void onOrder(OrderEvent orderEvent) { //synchronized is not good practice, but it simplifies the code snippet.
			var eventStatutes = receivedEvents.stream() //find all statuses for the same orderId and customerId
					.filter(o -> o.orderId().equals(orderEvent.orderId()) && o.customerId().equals(orderEvent.customerId()))
					.map(OrderEvent::status)
					.toList();

			receivedEvents.add(orderEvent);
			log.info("Event accepted {}", orderEvent);

			detectDuplicities(orderEvent, eventStatutes);
			detectOrderChange(orderEvent, eventStatutes);
		}

		/**
		 *  Detect if event DELIVERED is processed before event CREATED
		 */
		private static void detectOrderChange(OrderEvent orderEvent, List<OrderStatus> eventStatutes) {
			if (orderEvent.status() == OrderStatus.DELIVERED && !eventStatutes.contains(OrderStatus.CREATED)) {
				log.warn("Event order swapped. Order was {} before {}. Event {}", OrderStatus.DELIVERED, OrderStatus.CREATED, orderEvent);
			}
		}

		/**
		 * Detect if event was not already processed in the past
		 */
		private static void detectDuplicities(OrderEvent orderEvent, List<OrderStatus> eventStatutes) {
			if (eventStatutes.contains(orderEvent.status())) {
				log.warn("Duplicated event received {}", orderEvent);
			}
		}
	}

	@Bean
	public OrderingConsumerImpl orderingConsumerImpl() {
		return new OrderingConsumerImpl();
	}

	public static void main(String[] args) {
		SpringApplication.run(OrderingConsumer.class, args);
	}
}
