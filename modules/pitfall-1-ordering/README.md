# Ordering pitfall

## Idea of example

Kafka guarantees ordering of messages within the partition. Ordering between the partitions is undefined.
Producer doesn't define the message key, so kafka will select partition by default implementation. 

If we can prevent the swapping of DELIVERED and CREATED events for the order we need to route the all events of the order
to the same partition. Fix of producer is to assign message key:

```java
    kafkaTemplate.send("TOPIC", event); 
    kafkaTemplate.send("TOPIC", event.orderId(), event); //message key: event.orderId()
```

## How to use the example

1. Run producer
2. Run consumer

```
....
2023-12-30 16:23:06,393 WARN  Event order swapped. Order was DELIVERED before CREATED. Event OrderEvent[orderId=1, customerId=Customer6, status=DELIVERED]
....
2023-12-30 16:23:06,401 INFO  Event accepted OrderEvent[orderId=1, customerId=Customer6, status=CREATED]
....
```
