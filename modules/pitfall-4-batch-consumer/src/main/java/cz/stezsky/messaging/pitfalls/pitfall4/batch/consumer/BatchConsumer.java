package cz.stezsky.messaging.pitfalls.pitfall4.batch.consumer;

import cz.stezsky.messaging.pitfalls.api.OrderEvents.OrderEvent;
import cz.stezsky.messaging.pitfalls.bootstrap.consumer.KafkaConsumerConfiguration;
import cz.stezsky.messaging.pitfalls.pitfall4.batch.producer.BatchProducer;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.listener.DefaultErrorHandler;
import org.springframework.util.backoff.FixedBackOff;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@SpringBootApplication
@Configuration
@Import(KafkaConsumerConfiguration.class)
@Slf4j
public class BatchConsumer {

	@Slf4j
	public static class BatchConsumerImpl {
		private final AtomicInteger attempt = new AtomicInteger(0);
		@KafkaListener(topics = BatchProducer.TOPIC, groupId = "${random.uuid}", properties = {"max.poll.records:200"}, containerFactory = "kafkaListenerContainerFactory")
		public void onOrders(List<ConsumerRecord<String, OrderEvent>> messages) {
			log.info("Attempt #{} Batch received: {} in thread [{}]", attempt.incrementAndGet(), messages.size(), Thread.currentThread().getName());
			throw new IllegalArgumentException("Rollback");
		}
	}
	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory(ConsumerFactory consumerFactory) {
		DefaultErrorHandler der = new DefaultErrorHandler(new FixedBackOff(0, 1));
		ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(consumerFactory);
		factory.setBatchListener(true);
		factory.setCommonErrorHandler(der);
		return factory;
	}

	@Bean
	public BatchConsumerImpl bulkConsumerImpl() {
		return new BatchConsumerImpl();
	}

	public static void main(String[] args) {
		SpringApplication.run(BatchConsumer.class, args);
	}
}
