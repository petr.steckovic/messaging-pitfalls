package cz.stezsky.messaging.pitfalls.pitfall4.batch.producer;

import cz.stezsky.messaging.pitfalls.api.OrderEvents.OrderEvent;
import cz.stezsky.messaging.pitfalls.bootstrap.producer.EventFile;
import cz.stezsky.messaging.pitfalls.bootstrap.producer.KafkaProducerConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.core.KafkaTemplate;

import java.nio.file.Path;

import static cz.stezsky.messaging.pitfalls.bootstrap.producer.SendResultLogger.LOG_EVENT;

@SpringBootApplication
@Import(KafkaProducerConfiguration.class)
@Slf4j
public class BatchProducer implements CommandLineRunner {
	//kafkacat -b localhost:29092 -t pitfall-4-batch-1 -C -f '%p) %s\n'
	public static final String TOPIC = "pitfall-4-batch-1";
	//public static final String TOPIC = "pitfall-4-batch-2";
	//public static final String TOPIC = "pitfall-4-batch-3";

	@Autowired
	private KafkaTemplate<String, OrderEvent> kafkaTemplate;

	@Override
	public void run(String... args){
		//Read events from file
		EventFile.readEvents(Path.of("events", "100-events.csv")).forEach(event -> {
			//Send to kafka
			kafkaTemplate.send(TOPIC, event.customerId(), event).whenComplete(LOG_EVENT);
		});
	}

	public static void main(String[] args) {
		SpringApplication.run(BatchProducer.class, args);
	}
}
