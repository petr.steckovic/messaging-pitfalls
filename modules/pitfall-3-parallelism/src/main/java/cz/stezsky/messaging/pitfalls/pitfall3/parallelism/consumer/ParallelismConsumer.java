package cz.stezsky.messaging.pitfalls.pitfall3.parallelism.consumer;

import cz.stezsky.messaging.pitfalls.api.OrderEvents.OrderEvent;
import cz.stezsky.messaging.pitfalls.bootstrap.consumer.KafkaConsumerConfiguration;
import cz.stezsky.messaging.pitfalls.pitfall3.parallelism.producer.ParallelismProducer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.annotation.KafkaListener;

import java.util.concurrent.atomic.AtomicInteger;

@SpringBootApplication
@Configuration
@Import(KafkaConsumerConfiguration.class)
@Slf4j
public class ParallelismConsumer {

	@Slf4j
	public static class ParallelConsumerImpl {
		private final AtomicInteger runningThreads = new AtomicInteger(0);

		@KafkaListener(topics = ParallelismProducer.TOPIC, groupId = "${random.uuid}")
		public void onOrder(OrderEvent orderEvent) {
			try {
				log.info("Running threads {}", runningThreads.incrementAndGet());
				Thread.sleep(500);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			} finally {
				runningThreads.decrementAndGet();
			}
		}
	}
	@Bean
	public ParallelConsumerImpl parallelConsumerImpl() {
		return new ParallelConsumerImpl();
	}

	public static void main(String[] args) {
		SpringApplication.run(ParallelismConsumer.class, args);
	}
}
