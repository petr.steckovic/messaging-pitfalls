package cz.stezsky.messaging.pitfalls.pitfall3.parallelism.producer;

import cz.stezsky.messaging.pitfalls.api.OrderEvents.OrderEvent;
import cz.stezsky.messaging.pitfalls.bootstrap.producer.EventFile;
import cz.stezsky.messaging.pitfalls.bootstrap.producer.KafkaProducerConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.nio.file.Path;

import static cz.stezsky.messaging.pitfalls.bootstrap.producer.SendResultLogger.LOG_EVENT;

@SpringBootApplication
@Import(KafkaProducerConfiguration.class)
@EnableTransactionManagement
@Slf4j
public class ParallelismProducer implements CommandLineRunner {
	//kafkacat -b localhost:29092 -t pitfall-3-transactions-1-p2 -C -f '%p) %s\n'
	public static final String TOPIC = "pitfall-3-transactions-1-p2";

	//kafkacat -b localhost:29092 -t pitfall-3-transactions-2-p4 -C -f '%p) %s\n'
	//public static final String TOPIC = "pitfall-3-transactions-2-p4";

	//public static final String TOPIC = "pitfall-3-transactions-3-p4";

	@Autowired
	private KafkaTemplate<String, OrderEvent> kafkaTemplate;

	@Override
	public void run(String... args){
		//Read events from file
		EventFile.readEvents(Path.of("events", "100-events.csv")).forEach(event -> {
			//Send to kafka
			kafkaTemplate.send(TOPIC, event).whenComplete(LOG_EVENT);
		});
	}

	public static void main(String[] args) {
		SpringApplication.run(ParallelismProducer.class, args);
	}
}
