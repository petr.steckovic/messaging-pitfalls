package cz.stezsky.messaging.pitfalls.bootstrap.consumer;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.kafka.annotation.EnableKafka;

@Configuration
@EnableKafka
@PropertySource("classpath:/kafka-consumer.properties")
public class KafkaConsumerConfiguration {
}
