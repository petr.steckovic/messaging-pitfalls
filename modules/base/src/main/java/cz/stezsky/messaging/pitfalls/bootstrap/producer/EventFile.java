package cz.stezsky.messaging.pitfalls.bootstrap.producer;

import cz.stezsky.messaging.pitfalls.api.OrderEvents.OrderEvent;
import cz.stezsky.messaging.pitfalls.api.OrderEvents.OrderStatus;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static cz.stezsky.messaging.pitfalls.api.OrderEvents.OrderStatus.CREATED;
import static cz.stezsky.messaging.pitfalls.api.OrderEvents.OrderStatus.DELIVERED;
import static java.nio.file.StandardOpenOption.*;

@UtilityClass
public class EventFile {
    private static final Random RND = new Random();


    /***
     * Read events from csv file with format:
     * orderId,customerId,OrderStatus
     * @param eventFile
     * @return
     */
    @SneakyThrows
    public static List<OrderEvent> readEvents(Path eventFile) {
        return Files.readAllLines(eventFile).stream().filter(l -> !l.isBlank())
                .map(l -> l.split(","))
                .map(a -> new OrderEvent(a[0].trim(), a[1].trim(), OrderStatus.valueOf(a[2].trim())))
                .toList();
    }

    /**
     * Save (or overwrite) event csv file
     * @param eventFile
     * @param numberOfOrders
     * @param numberOfCustomers
     */
    @SneakyThrows
    public static void generateEventFile(Path eventFile, int numberOfOrders, int numberOfCustomers) {
        var csv = generateMessages(numberOfOrders, numberOfCustomers).stream()
                .map(o -> o.orderId() + "," + o.customerId() + "," + o.status() + "\n")
                .collect(Collectors.joining());
        Files.writeString(eventFile, csv, CREATE, TRUNCATE_EXISTING, WRITE);
    }


    private static List<OrderEvent> generateMessages(int numberOfOrders, int numberOfCustomers) {
        var result = new ArrayList<OrderEvent>();
        var consumers = new HashMap<String, AtomicInteger>();
        var deliveredOrders = new LinkedList<OrderEvent>();

        for (int i = 0; i < numberOfOrders; i++) {
            String consumerId = "Customer" + RND.nextInt(0,numberOfCustomers);
            int orderNo = consumers.computeIfAbsent(consumerId, k -> new AtomicInteger(0)).incrementAndGet();
            result.add(new OrderEvent(String.valueOf(orderNo), consumerId, CREATED));
            deliveredOrders.add(new OrderEvent(String.valueOf(orderNo), consumerId, DELIVERED));

            if (RND.nextInt(0, 4) % 4 == 0) { //send 25% orders immediately
                result.add(deliveredOrders.poll());
            }
        }
        result.addAll(deliveredOrders);
        return result;
    }

    public static void main(String[] args) {
        generateEventFile(Path.of("./events/100-events.csv"), 50, 10);
    }
}
