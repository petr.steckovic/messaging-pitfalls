package cz.stezsky.messaging.pitfalls.api;

public interface OrderEvents {

    record OrderEvent(String orderId, String customerId, OrderStatus status) {}

    enum OrderStatus {
        CREATED,

        DELIVERED
    }
}
