package cz.stezsky.messaging.pitfalls.bootstrap.producer;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.support.SendResult;

import java.util.function.BiConsumer;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SendResultLogger implements BiConsumer<SendResult<?, ?>, Throwable> {
    public static final SendResultLogger LOG_EVENT = new SendResultLogger();

    @Override
    public void accept(SendResult result, Throwable ex) {
        if (ex != null) {
            log.error("Unable to send message {}.", result.getProducerRecord().value(), ex);
        } else {
            log.info("Message {} sent successfully to partition {} ", result.getProducerRecord().value(), result.getRecordMetadata().partition());
        }
    }
}
