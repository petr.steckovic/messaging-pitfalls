package cz.stezsky.messaging.pitfalls.bootstrap.producer;


import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.kafka.annotation.EnableKafka;

@Configuration
@EnableKafka
@PropertySource("classpath:/kafka-producer.properties")
public class KafkaProducerConfiguration {
}
