# Idempotence

1. Producer creates unique message ID (UUID) and sends it in message.
2. Consumer has a storage of consumed messages. If message is found in storage message is considered as idempotent 