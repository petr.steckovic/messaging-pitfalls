package cz.stezsky.messaging.pitfalls.pitfall6.idempotence.producer;

import cz.stezsky.messaging.pitfalls.api.OrderEvents.OrderEvent;
import cz.stezsky.messaging.pitfalls.bootstrap.producer.EventFile;
import cz.stezsky.messaging.pitfalls.bootstrap.producer.KafkaProducerConfiguration;
import cz.stezsky.messaging.pitfalls.pitfall6.idempotence.consumer.ProcessedMessageRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.core.KafkaTemplate;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;
import java.util.UUID;

import static cz.stezsky.messaging.pitfalls.bootstrap.producer.SendResultLogger.LOG_EVENT;

@SpringBootApplication
@Import(KafkaProducerConfiguration.class)
@Slf4j
public class IdempotentProducer implements CommandLineRunner {
	//kafkacat -b localhost:29092 -t pitfall-6-idempotence-1 -C -f '%p) %s\n'
	public static final String TOPIC = "pitfall-6-idempotence-1";
	public static final String MESSAGE_ID_HEADER = "MESSAGE_ID";
	//public static final String TOPIC = "pitfall-6-idempotence-2";
	//public static final String TOPIC = "pitfall-6-idempotence-3";

	@Autowired
	private KafkaTemplate<String, OrderEvent> kafkaTemplate;


	@Override
	public void run(String... args){
		//Read events from file
		EventFile.readEvents(Path.of("events", "5-events.csv")).forEach(event -> {
			//Generate messageId
			List<Header> messageIdHeader = List.of(new RecordHeader(MESSAGE_ID_HEADER,
					UUID.randomUUID().toString().getBytes(StandardCharsets.UTF_8)));
			//Send to kafka
			kafkaTemplate.send(new ProducerRecord<>(TOPIC, null, event.customerId(), event, messageIdHeader)).whenComplete(LOG_EVENT);
			//Send duplicity
			kafkaTemplate.send(new ProducerRecord<>(TOPIC, null, event.customerId(), event, messageIdHeader)).whenComplete(LOG_EVENT);
		});
	}

	public static void main(String[] args) {
		SpringApplication.run(IdempotentProducer.class, args);
	}
}
