package cz.stezsky.messaging.pitfalls.pitfall6.idempotence.consumer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProcessedMessageRepository extends JpaRepository<ProcessedMessage, String> {

    @Modifying
    @Query(value = "INSERT INTO processed_message (message_id, created) VALUES (:#{#message.messageId}, :#{#message.created})", nativeQuery = true)
    void insert(@Param("message") ProcessedMessage message);
}
