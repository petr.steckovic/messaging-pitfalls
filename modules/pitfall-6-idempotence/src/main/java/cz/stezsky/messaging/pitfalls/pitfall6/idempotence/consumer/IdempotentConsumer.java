package cz.stezsky.messaging.pitfalls.pitfall6.idempotence.consumer;

import cz.stezsky.messaging.pitfalls.api.OrderEvents.OrderEvent;
import cz.stezsky.messaging.pitfalls.bootstrap.consumer.KafkaConsumerConfiguration;
import cz.stezsky.messaging.pitfalls.pitfall6.idempotence.producer.IdempotentProducer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.listener.RecordInterceptor;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;

@SpringBootApplication
@Configuration
@Import(KafkaConsumerConfiguration.class)
@EnableTransactionManagement
@Slf4j
@ComponentScan
public class IdempotentConsumer {


	@Slf4j
	@RequiredArgsConstructor
	@Component
	public static class ConsumerImpl {

		@Transactional
		@KafkaListener(topics = IdempotentProducer.TOPIC, groupId = "${random.uuid}", containerFactory = "kafkaListenerContainerFactory")
		public void onOrders(OrderEvent event, @Header(IdempotentProducer.MESSAGE_ID_HEADER) String messageId) {
			log.info("{} delivery {}", messageId, event);
		}
	}


	@Slf4j
	@RequiredArgsConstructor
	private static class IdempotentRecordInterceptor<K, V> implements RecordInterceptor<K, V> {
		private final DuplicateMessageDetector duplicateMessageDetector;
		@Override
		@Transactional
		public ConsumerRecord<K, V> intercept(ConsumerRecord<K, V> record, Consumer<K, V> consumer) {
			var messageIdHeader = record.headers().lastHeader(IdempotentProducer.MESSAGE_ID_HEADER);
			if (messageIdHeader != null) {
				String messageId = new String(messageIdHeader.value(), StandardCharsets.UTF_8);
				if (duplicateMessageDetector.isDuplicatedMessage(messageId)) {
					log.warn("Duplicate message detected {}", record.value());
					//Swallow message
					return null;
				}
			}
			return record;
		}
	}

	@Bean
	public ConcurrentKafkaListenerContainerFactory kafkaListenerContainerFactory(ConsumerFactory consumerFactory, DuplicateMessageDetector duplicateMessageDetector) {
		ConcurrentKafkaListenerContainerFactory<?, ?> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(consumerFactory);
		factory.setRecordInterceptor(new IdempotentRecordInterceptor<>(duplicateMessageDetector));
		return factory;
	}

	public static void main(String[] args) {
		SpringApplication.run(IdempotentConsumer.class, args);
	}
}
