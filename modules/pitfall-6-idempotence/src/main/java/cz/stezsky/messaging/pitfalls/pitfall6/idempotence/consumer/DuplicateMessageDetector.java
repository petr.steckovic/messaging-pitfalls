package cz.stezsky.messaging.pitfalls.pitfall6.idempotence.consumer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Component
public class DuplicateMessageDetector {
    private final ProcessedMessageRepository repo;

    @Transactional
    public boolean isDuplicatedMessage(String messageId) {
        if (!repo.existsById(messageId)) {
            repo.insert(new ProcessedMessage(messageId));
            return false;
        }  else {
            return true;
        }
    }
}
