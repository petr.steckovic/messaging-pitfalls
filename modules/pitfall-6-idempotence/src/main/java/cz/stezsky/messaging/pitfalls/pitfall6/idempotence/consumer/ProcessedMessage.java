package cz.stezsky.messaging.pitfalls.pitfall6.idempotence.consumer;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProcessedMessage {

    @Id
    private String messageId;

    //will be used for retention
    private Instant created = Instant.now();

    public ProcessedMessage(String messageId) {
        this.messageId = messageId;
    }
}
