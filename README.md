# Kafka pitfalls

Repository contains examples of pitfalls of kafka messaging. 

## Run kafka cluster ##
```shell
make up
```

or simply

```shell
make
```

Kafka cluster have 2 nodes on addresses `localhost:39092` and `localhost:29092`. [Kouncil](https://kouncil.io/) GUI is available on `http://localhost:18080/`  

## Kafkacat tool

[Tools](https://www.root.cz/clanky/prace-s-kafkou-z-prikazove-radky-nastroje-kafkacat-a-kcli/) for kafka 

**Installation**
```shell
sudo apt install kafkacat
``` 

**List metadata**

```shell
kafkacat -L -b localhost:29092
```


# Kafka cat hacks

```shell
# List all messages in topic
kafkacat -b localhost:29092 -t pitfall-1-ordering-2 -C -f '%p) %s\n'
         
```


# Resources 
* [Main doc](https://kafka.apache.org/documentation/)
* [Apache Kafka - Architecture and Components](https://medium.com/featurepreneur/apache-kafka-architecture-and-components-147dcd1045ba)
* [Understanding partitions](https://medium.com/event-driven-utopia/understanding-kafka-topic-partitions-ae40f80552e8)
* [Effective strategies kafka topic partitions](https://newrelic.com/blog/best-practices/effective-strategies-kafka-topic-partitioning)
* [Writing custom partitioner](https://www.clairvoyant.ai/blog/writing-custom-partitioner-for-apache-kafka)
* [Processing guarantees in kafka](https://medium.com/@andy.bryant/processing-guarantees-in-kafka-12dd2e30be0e)