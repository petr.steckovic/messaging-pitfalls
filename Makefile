DC	:= docker-compose --project-name messaging-pitfalls --project-directory ./docker-compose

up:
	$(DC) up --detach

down:
	$(DC) down --remove-orphans

reinstall: down up


